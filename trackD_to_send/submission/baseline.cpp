// Copyright 2019, Nikita Kazeev, Higher School of Economics
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <iterator>
#include <vector>
#include <limits>

#include "./parser.h"
#include "ripped_evaluator/evaluator.h"

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(NULL);
    const std::string MODEL_FILE = "track_2_model.cbm";
    NCatboostStandalone::TOwningEvaluator evaluator(MODEL_FILE);
    // Skip header
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << std::setprecision(std::numeric_limits<float>::max_digits10);
    std::cout << "id,prediction\n";
    int arr[] = {8, 12, 14, 15, 16, 18, 19, 20, 22, 23, 24, 28, 30, 31, 32, 36, 39, 40, 45, 46, 47, 48, 49, 50, 51, 52, 55, 56, 57, 58, 59, 60, 61, 75, 87};
    while (std::cin.good() && std::cin.peek() != EOF) {
	std::vector<float> features(N_FEATURES);
        size_t id;
        ugly_hardcoded_parse(std::cin, &id, &features);
        for (size_t i = 0; i < 35; i++){
            features.erase (features.begin()+arr[i]-i);
        }
        // for (int i=0; i < 72; i++) {
        //     std::cout<<i << " " << features[i] <<std::endl;
        // } 
        // break;
        const float prediction = 
            evaluator.Apply(features, NCatboostStandalone::EPredictionType::RawValue);
        std::cout << id << DELIMITER << prediction  << '\n';
    }
    return 0;
}
